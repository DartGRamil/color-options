<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'get_icon') {
    header ("Content-type: image/png");
    $image = imagecreatetruecolor(31, 31);
    $_REQUEST['color'] = !empty($_REQUEST['color']) ? $_REQUEST['color'] : '000000';
    list($r, $g, $b) = fn_color_options_get_rgb($_REQUEST['color']);
    $color = imagecolorallocate($image, $r, $g, $b);
    imagefill($image, 1, 1, $color);
    imagerectangle($image, 0, 0, 31, 31, $color);

    imagepng($image);
    imagedestroy($image);
    exit; 
}
