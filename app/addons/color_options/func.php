<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_color_options_get_rgb($color = '000000')
{
    $r = $g = $b = '0xff';
    if (strlen($color) == 6) {
        $r = '0x' . $color[0] . $color[1];
        $g = '0x' . $color[2] . $color[3];
        $b = '0x' . $color[4] . $color[5];
    }

    return array($r, $g, $b);
}

// hooks

function fn_color_options_get_product_option_data_pre($option_id, $product_id, $fields, $condition, $join, &$extra_variant_fields)
{
    $extra_variant_fields .= "a.color,";
}

function fn_color_options_gather_additional_products_data_post($product_ids, $params, &$products, $auth)
{
    $option_color = 0;
    foreach ($products as &$product) {
        if (!empty($product['product_options'])) {
            foreach ($product['product_options'] as &$product_option) {
                if ($product_option['is_color'] == 'Y') {
                    $defaul_color = '';
                    foreach ($product_option['variants'] as $key => &$variant) {
                        if (!empty($variant['option_id'])) {
                            $variant['color'] = db_get_field("SELECT color FROM ?:product_option_variants WHERE variant_id = ?i AND option_id = ?i", $variant['variant_id'], $variant['option_id']);
                            if (empty($defaul_color)) $defaul_color = $variant['color'];
                        }
                    }
                    $product_option['defaul_color'] = $defaul_color;
                }
            }
        }
    }
}
