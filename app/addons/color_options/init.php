<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'get_product_option_data_pre',
    'gather_additional_products_data_post'
);