{if $option_data.is_color == "Y"}
    <div class="control-group">
        <label class="control-label">{__("color")}:</label>
        <div class="controls">
            <input type="text" name="option_data[variants][{$num}][color]" value="{$vr.color|default:''}" size="5" class="input-mini color-cod" id="colorpickerField{$num}" />
        </div>
    </div>
{/if}