<div class="control-group">
    <label class="control-label" for="elm_is_color_{$id}">{__("is_color")}</label>
    <div class="controls">
    <label class="checkbox">
        <input type="hidden" name="option_data[is_color]" value="N" />
        <input type="checkbox" id="elm_is_color_{$id}" name="option_data[is_color]" value="Y" {if $option_data.is_color == "Y"}checked="checked"{/if}  />
    </label>
    </div>
</div>
{if $option_data.is_color == "Y"}
    <script type="text/javascript">
        (function(_, $){
            $('.color-cod').click(function() {
                $(this).ColorPicker({
                    onSubmit: function(hsb, hex, rgb, el) {
                        $(el).val(hex);
                        $(el).ColorPickerHide();
                    },
                    onBeforeShow: function () {
                        $(this).ColorPickerSetColor(this.value);
                        }
                }).bind('keyup', function(){
                    $(this).ColorPickerSetColor(this.value);
                });
            });
        })(Tygh, Tygh.$);
    </script>
{/if}